SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE test;

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(8) NOT NULL,
  `name` varchar(20) NOT NULL,
  `age` int(3) NOT NULL,
  `isAdmin` bit(1) NOT NULL,
  `createdDate` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) TYPE=InnoDB;

INSERT INTO `user` (`id`, `name`, `age`, `isAdmin`, `createdDate`) VALUES
(1, 'Ivan', 22, b'1', '2016-02-09 19:09:10'),
(2, 'Pavel', 24, b'0', '2016-02-09 19:42:43'),
(3, 'Boris', 24, b'0', '2016-02-09 19:42:44'),
(4, 'James', 24, b'0', '2016-02-09 19:42:44'),
(5, 'Frank', 48, b'1', '2016-02-09 19:47:08'),
(6, 'Peter', 24, b'0', '2016-02-09 19:42:44'),
(7, 'Arnold', 24, b'0', '2016-02-09 19:42:44'),
(8, 'Donatello', 24, b'0', '2016-02-09 19:42:44'),
(9, 'Stewart', 24, b'0', '2016-02-09 19:42:44'),
(10, 'Don kihot', 24, b'0', '2016-02-09 19:42:44'),
(11, 'Andy', 24, b'0', '2016-02-09 19:42:44'),
(12, 'Pavel', 24, b'0', '2016-02-09 19:42:44'),
(13, 'Boris', 24, b'0', '2016-02-09 19:42:44'),
(14, 'Jackson', 27, b'1', '2016-02-09 19:47:36'),
(15, 'Ivan', 24, b'0', '2016-02-09 19:42:44'),
(16, 'Peter', 24, b'0', '2016-02-09 19:42:44'),
(17, 'Arnold', 24, b'0', '2016-02-09 19:42:44'),
(18, 'Donatello', 24, b'0', '2016-02-09 19:42:44'),
(19, 'Stewart', 24, b'0', '2016-02-09 19:42:44'),
(20, 'Don kihot', 24, b'0', '2016-02-09 19:42:44'),
(21, 'Andy', 24, b'0', '2016-02-09 19:42:44'),
(22, 'Pavel', 24, b'0', '2016-02-09 19:42:44'),
(23, 'Boris', 24, b'0', '2016-02-09 19:42:44'),
(24, 'James', 24, b'0', '2016-02-09 19:42:44'),
(25, 'Ivan', 24, b'0', '2016-02-09 19:42:44'),
(26, 'Peter', 24, b'0', '2016-02-09 19:42:44'),
(28, 'Donatello', 24, b'0', '2016-02-09 19:42:44'),
(29, 'McLaren', 24, b'1', '2016-02-09 19:58:20');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
