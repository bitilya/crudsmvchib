package com.ilya;

import java.util.Date;

/**
 * Created by alik72908 on 04.02.2016.
 */
public class User {

    public User(){}
    public User(User us){
        this.id=us.getId();
        this.name = new String(us.getName());
        this.age = us.getAge();
        this.createdDate =(Date) us.getCreatedDate().clone();
    }

    private int id;
    private String name;
    private int age;
    private boolean isAdmin;
    private Date createdDate;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append(", isAdmin=").append(isAdmin);
        sb.append(", createdDate=").append(createdDate);
        sb.append('}');
        return sb.toString();
    }
}
