package com.ilya.utils;

import com.ilya.User;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by alik72908 on 07.02.2016.
 */
public class UsersFormValidator implements Validator {

    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    public void validate(Object target, Errors errors) {
        User user = (User) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"name","1","Name is required !");
        if(user.getName().matches("^.*\\d+.*$")){
            errors.rejectValue("name","2","No name contains numbers");
        }
        if(user.getName().length()>20){
            errors.rejectValue("name","4","Too long name");
        }
        if(user.getAge()<0 || user.getAge()>150){
            errors.rejectValue("age","3","Age must be real");
        }
    }
}
