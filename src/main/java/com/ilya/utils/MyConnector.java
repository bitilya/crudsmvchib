package com.ilya.utils;

import com.ilya.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by alik72908 on 05.02.2016.
 */
public class MyConnector {

    public static List<User> getAllUsers(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = session.beginTransaction();
        List<User> list =  session.createCriteria(User.class).list();
        ts.commit();
        return list;
    }

    public static User getById(int id){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = session.beginTransaction();
        User user =  session.load(User.class, id);
        ts.commit();
        return user;
    }

    public  static void addUser(User user){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.save(user);
        session.flush();
        session.close();
    }

    public static void deleteUser(int id){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = session.beginTransaction();
        User user = (User) session.load(User.class,id);
        session.delete(user);
        ts.commit();
    }

    public static void updateUser(User user){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = session.beginTransaction();
        session.update(user);
        ts.commit();
    }
    public static List<User> selectByName(String name){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = session.beginTransaction();
        Criteria cr = session.createCriteria(User.class);
        cr.add(Restrictions.ilike("name", name));
        List<User> list = cr.list();
        ts.commit();
        return list;
    }

    public static List<User> getPageUsers(int ind){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = session.beginTransaction();
        Criteria cr = session.createCriteria(User.class);
        cr.setMaxResults(5);
        cr.setFirstResult(ind);
        List<User> list = cr.list();
        ts.commit();
        return list;
    }

    public static long receiveSizeTotal(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = session.beginTransaction();
        Criteria cr = session.createCriteria(User.class);
        cr.setProjection(Projections.rowCount());
        long a = (Long) cr.list().get(0);
        ts.commit();
        return a;
    }

}
