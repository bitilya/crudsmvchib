package com.ilya.controller;


import com.ilya.User;
import com.ilya.utils.MyConnector;
import com.ilya.utils.UsersFormValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
public class AppController {

    @InitBinder("User")
    protected void initBinder(WebDataBinder binder){
        binder.setValidator(new UsersFormValidator());
    }

    @RequestMapping(value = "/list")
    public ModelAndView alternateListUsers(@RequestParam(value = "map", required = false) String map,HttpSession sess){
       if(sess.getAttribute("index")==null){
           sess.setAttribute("index",0);
           sess.setAttribute("total",MyConnector.receiveSizeTotal());
       }
       if("n".equals(map)){
           sess.setAttribute("index",(Integer)(sess.getAttribute("index"))+5);
       }
        if("p".equals(map)){
            sess.setAttribute("index",(Integer)(sess.getAttribute("index"))-5);
        }
        ModelAndView m = new ModelAndView("loop");
        List<User> list = MyConnector.getPageUsers((Integer)sess.getAttribute("index"));
        for (User u : list){
            System.out.println(u);
        }
        m.addObject("listUsers", list);
        return m;
    }

//    @RequestMapping(value = "/list/ss", method = RequestMethod.GET)
//    public ModelAndView listUsers(){
//      ModelAndView m = new ModelAndView("loop");
//      List<User> list = MyConnector.getPageUsers(0);
//      for (User u : list){
//          System.out.println(u);
//      }
//      m.addObject("listUsers", list);
//      return m;
//    }

    @RequestMapping(value = "/userAdd", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("User") @Validated User user,BindingResult result ,Model model,HttpSession sess){
        if(result.hasErrors()){
            List<User> list = MyConnector.getPageUsers((Integer)sess.getAttribute("index"));
            model.addAttribute("listUsers",list);
            return "/loop";
        }
        user.setCreatedDate(new Date());
        if(user.getId()==0){
            MyConnector.addUser(user);
            sess.setAttribute("total", MyConnector.receiveSizeTotal());
        }
        else MyConnector.updateUser(user);
        return "redirect:/list";
    }

    @RequestMapping(value="/remove/{id}")
    public String deleteUser(@PathVariable("id") int id,HttpSession sess){
        MyConnector.deleteUser(id);
        sess.setAttribute("total", MyConnector.receiveSizeTotal());
        return "redirect:/list";
    }

    @RequestMapping(value="/edit/{id}")
    public ModelAndView getUserbyId(@PathVariable("id")int id,HttpSession sess){
        ModelAndView m = new ModelAndView("loop");
        User u = new User(MyConnector.getById(id));
        m.addObject("User",u);
        List<User> list = MyConnector.getPageUsers((Integer)sess.getAttribute("index"));
        m.addObject("listUsers", list);
        return m;
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String selectByName(@RequestParam("name") String name, Model model){
        model.addAttribute("listUsers", MyConnector.selectByName(name));
        model.addAttribute("goback",0);
        return "/loop";
    }

    @ModelAttribute("User")
    public User emptyBean(){
        return new User();
    }
}
