<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="true" isELIgnored="false" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <jsp:include page="../view/header.jsp" />
</head>
<body>
<h1>
    Add a Person
</h1>
<nav>
<c:url var="addAction" value="/userAdd" ></c:url>
<form:form class="form-horizontal" modelAttribute="User" action="${addAction}" method = "post">
<table>
           <c:if test="${User.id !=0}">
    <tr>
        <td><label for="id"> ID</label></td>
        <td><input type="text" name="id" readonly="true" size="8" value="${User.id}" /></td>
    </tr>
            </c:if>
    <tr>

        <td><label path="name">Name</label></td>
        <td><form:input type="text" path="name" size="8" value="${User.name}" /></td>
        <td><form:errors path="name" class="control-label" /></td>
        </div>
    </tr>
    <tr>

        <td><label path="age"> Age</label></td>
        <td><form:input type="text" path="age" size="8" value="${User.age}"/></td>
        <td><form:errors path="age" class="control-label" /></td>

    </tr>
    <tr>
         <td> <label path="isAdmin"> Administrator </label></td>
         <td><input type="radio" name="isAdmin" size="3" value="true" /> </td>
    </tr>
    <tr>
           <td> <label path="isAdmin">Not Administrator</label></td>
           <td><input type="radio" name="isAdmin" size="3" value="false"/></td>
     </tr>
    <tr>
        <td colspan="2"><c:if test="${ User.id !=0}"> <input type="submit"value="Edit Person"/>
                        </c:if>
                        <c:if test="${ User.id ==0}"><input type="submit" value="Add Person"/>
                        </c:if>
        </td>
    </tr>
</table>
</form:form>
<br>
<c:url var="searchUs" value="/search" ></c:url>
<form action="${searchUs}" method="post">
<label>Select by Name</label> <input type="text" name="name" size="8" /></br>
<input type="submit" value ="Submit">
</form>
</nav>
<section>
<h2 align="center">Persons List</h2>
<c:if test="${!empty listUsers}">
    <table class="tg" align = "center">
    <tr>
        <th width="50">Person ID</th>
        <th width="80">Person Name</th>
        <th width="80">Person Age</th>
        <th width="50">isAdmin</th>
        <th width="100">createdDate</th>
        <th width="60">Edit</th>
        <th width="60">Delete</th>
    </tr>
    <c:forEach items="${listUsers}" var="User">
        <tr>
            <td>${User.id}</td>
            <td>${User.name}</td>
            <td>${User.age}</td>
            <td>${User.isAdmin}</td>
            <td>${User.createdDate}</td>
            <td><a href="<c:url value='/edit/${User.id}' />" >Edit</a></td>
            <td><a href="<c:url value='/remove/${User.id}' />" >Delete</a></td>
        </tr>
    </c:forEach>
    </table>
    <h3 align="center"><c:if test="${sessionScope.index != 0 && empty goback}"><a href = "<c:url value='/list?map=p'/>">Previous</a></c:if>
                       <c:if test="${sessionScope.index+5 < sessionScope.total && empty goback}"><a href = "<c:url value='/list?map=n'/>"> Next</a></c:if>
                       <c:if test="${! empty goback}"><a href = "<c:url value='/list'/>"> Go back</a></c:if>  </h3>
</c:if>
   <h3 align="center"><c:if test="${empty listUsers && ! empty sessionScope.total && sessionScope.total!=0 }"><a href = "<c:url value='/list?map=p'/>">Previous</a></c:if></h3>
</section>
</body>
</html>